<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::get('/', function () {
        return view('landingpage.index');
    });

    Route::get('/login', function () {
        return view('auth.login');
    });

    Route::get('/register', function () {
        return view('auth.register');
    });
});


// Route::resource('kategori', 'KategoriController');

// Route::resource('post', 'PostController');

// Route::resource('pertanyaan', 'PertanyaanController');


Route::middleware(['auth'])->group(function () {
    // KategoriController
    Route::get('/kategori/mycategory', 'KategoriController@mycategory');
    Route::resource('kategori', 'KategoriController');
    
    
    // PostController
    Route::get('/post/myquestion', 'PostController@myquestion');
    Route::resource('post', 'PostController');

    // Route::resource('pertanyaan', 'PertanyaanController');
    Route::resource('profile', 'ProfileController')->only('index', 'show', 'update');



    // Comment
    Route::resource('comment', 'CommentController');
    Route::post('/comment', 'CommentController@comment');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('kategori');

Route::post('upload', 'PostController@upload');


<!DOCTYPE html>
<html lang="en" id="top">

<head>
    <meta charset="utf-8">
    <title>
        Q&A Sanbercode
    </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    {{-- <link href="img/favicon.ico" rel="icon"> --}}

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset("/landingpage/lib/animate/animate.min.css") }}" rel="stylesheet">
    <link href="{{ asset("/landingpage/lib/owlcarousel/assets/owl.carousel.min.css") }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset("/landingpage/css/bootstrap.min.css") }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset("/landingpage/css/style.css") }}" rel="stylesheet">
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <!-- Spinner End -->


    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg bg-white navbar-light shadow sticky-top p-0">
        <a href="#" class="navbar-brand">
            <img src="{{ asset('/landingpage/img/logo/logov2.png') }}" alt="" width="300px" class="img-fluid">
        </a>
        <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">
                <a href="/login" class="nav-item nav-link">Login</a>
                <a href="/register" class="nav-item nav-link">SignUp</a>
                <a href="#aboutuspoint" class="nav-item nav-link">About us</a>
                <a href="#categoriespoint" class="nav-item nav-link">Categories</a>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->


    <!-- Carousel Start -->
    <div class="container-fluid p-0 mb-5">
        <div class="owl-carousel header-carousel position-relative">
            <div class="owl-carousel-item position-relative">
                <img class="img-fluid" src="{{ asset("/landingpage/img/carousel/carousel1.png") }}" alt="">
                <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(24, 29, 56, .7);">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-sm-10 col-lg-8">
                                <h2 class="display-3 text-white animated slideInDown mb-4">
                                    Find the best answers to your technical questions
                                </h2>
                                <a href="" class="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft">Join the Community</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="owl-carousel-item position-relative">
                <img class="img-fluid" src="{{ asset("/landingpage/img/carousel/carousel2.png") }}" alt="">
                <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(24, 29, 56, .7);">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-sm-10 col-lg-8">
                                <h2 class="display-3 text-white animated slideInDown">
                                    Each developer has an open tab for sharing between developers
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="owl-carousel-item position-relative">
                <img class="img-fluid" src="{{ asset("/landingpage/img/carousel/carousel3.png") }}" alt="">
                <div class="position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center" style="background: rgba(24, 29, 56, .7);">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-sm-10 col-lg-8">
                                <h1 class="display-3 text-white animated slideInDown">
                                    A public platform that builds a definitive set of coding questions & answers
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span id="aboutuspoint"></span>
    </div>
    <!-- Carousel End -->

    <!-- About Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                <h6 class="section-title bg-white text-center text-primary px-3">About us</h6>
                <h1 class="mb-5">Q&A SANBERCODE</h1>
            </div>

            <div class="row g-5">
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s" style="min-height: 400px;">
                    <div class="position-relative h-100">
                        <img class="img-fluid position-absolute w-100 h-100" src="{{ asset("/landingpage/img/test.png") }}" alt="" style="object-fit: cover;">
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.3s">
                <P>
                    Our platform is there to help you programmers to help each other in solving a problem related to the programming world, therefore we will be very happy if our platform that slides on the internet can help you
                </P>
                    <div class="row gy-2 gx-4 mb-4">
                        <div class="col-sm-12">
                            <p class="mb-0"><i class="fa fa-arrow-right text-primary me-2"></i>
                                quality forum
                            </p>
                        </div>
                        <div class="col-sm-12">
                            <p class="mb-0"><i class="fa fa-arrow-right text-primary me-2"></i>
                                Help each other between developers
                            </p>
                        </div>
                        <div class="col-sm-12">
                            <p class="mb-0"><i class="fa fa-arrow-right text-primary me-2"></i>
                                programmer library
                            </p>
                        </div>
                        <div class="col-sm-12">
                            <p class="mb-0"><i class="fa fa-arrow-right text-primary me-2"></i>
                                connect to programmers in different countries
                            </p>
                        </div>
                    </div>
                    <a class="btn btn-primary py-3 px-5 mt-2" href="#">Read more</a>
                </div>
            </div>
        </div>
        <span id="categoriespoint"></span>
    </div>
    <!-- About End -->

    <!-- Category Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                <h6 class="section-title bg-white text-center text-primary px-3">Categories</h6>
                <h2 class="mb-5">Here are just a few types of technologists that we help.</h2>
            </div>
            <div class="row g-4">
                <div class="col-lg-6 col-sm-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-item text-center pt-3">
                        <div class="p-4">
                            <img src="{{ asset('landingpage/img/category/1.png') }}" alt="" class="w-25 mb-4">
                            <h5 class="mb-3">FrontEnd Engineers</h5>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit laboriosam architecto nostrum nihil amet ipsam?</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="service-item text-center pt-3">
                        <div class="p-4">
                            <img src="{{ asset('landingpage/img/category/2.png') }}" alt="" class="w-25 mb-4">
                            <h5 class="mb-3">Backend Engineers</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et, mollitia! Sint, sed dolore? Error, tempore.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="service-item text-center pt-3">
                        <div class="p-4">
                            <img src="{{ asset('landingpage/img/category/3.png') }}" alt="" class="w-25 mb-4">
                            <h5 class="mb-3">DevOps Engineers</h5>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis illum, laboriosam hic incidunt odit quisquam.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 wow fadeInUp" data-wow-delay="0.7s">
                    <div class="service-item text-center pt-3">
                        <div class="p-4">
                            <img src="{{ asset('landingpage/img/category/4.png') }}" alt="" class="w-25 mb-4">
                            <h5 class="mb-3">Management and Support</h5>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ad autem doloribus vel sit accusantium architecto!</p>
                        </div>
                    </div>
                </div>
                {{-- About us point ----------------------- --}}
                <span id="aboutUsPoint"></span>
            </div>
        </div>
    </div>
    <!-- Category End -->

    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-light footer mt-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container">
            <div class="copyright p-0">
                <div class="row">
                    <div class="col-md-12 text-center text-md-start mb-3 mb-md-0">
                        <p class="text-center p-3 m-0">All rights reserved &copy; Copyright 2022. Created With <i class="far fa-heart text-danger"></i> Q&A Sanbercode</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#top" id="btn-to-top" class="btn btn-lg btn-lg-square back-to-top text-light"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset("landingpage/lib/wow/wow.min.js") }}"></script>
    <script src="{{ asset("landingpage/lib/easing/easing.min.js") }}"></script>
    <script src="{{ asset("landingpage/lib/waypoints/waypoints.min.js") }}"></script>
    <script src="{{ asset("landingpage/lib/owlcarousel/owl.carousel.min.js") }}"></script>

    <!-- Template Javascript -->
    <script src="{{ asset("landingpage/js/main.js") }}"></script>
</body>

</html>
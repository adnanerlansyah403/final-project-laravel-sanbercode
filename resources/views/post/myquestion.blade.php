@extends('layouts.master')

@push('styles')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
@endpush

@section('menu-table')
    Categories
@endsection

@section('menu-form')
    Question
@endsection

@section('content')
    <div class="card">
        <div class="card-header" style="background-color: #06bbcc">
            <h3 class="mb-0 text-light pt-2 pb-2">My Question List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Question</th>
                        <th>Kategori</th>
                        <th>Action</th>
                        {{-- <th style="width: 10%">Created_at</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($post as $item)
                        {{-- {{ dd($item->kategori->name) }} --}}
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $item->judul }}</td>
                            <td>{!! Str::limit($item->pertanyaan, 50, $end='...') !!}</td>
                            {{-- <td>{!! $item->pertanyaan !!}</td> --}}
                            <td>{{ $item->kategori->nama }}</td>
                            <td>
                                <a href="/post/{{ $item->id }}/edit" class="btn btn-warning btn-sm mb-1 btn-block text-light">Edit</a>
                                <form action="/post/{{ $item->id }}" method="POST">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    {{-- <button class="show_confirm btn btn-danger btn-sm mb-1 btn-block text-light" type="submit">Delete</button> --}}
                                    {{-- <button type="submit" class="btn btn-xs btn-danger btn-flat show_confirm" data-toggle="tooltip" title='Delete'>Delete</button> --}}
                                    <button type="submit" class="show_confirm btn btn-danger btn-sm mb-1 btn-block text-light" data-toggle="tooltip" title='Delete'>Delete</button>
                                </form>
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

@push('scripts')

<script src="{{ asset('themes/AdminLTE/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('themes/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script>
    
    $(function () {
        $("#example1").DataTable();
    });

    $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this question?`,
              text: "If you delete this, it will be gone forever.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });

</script>
@endpush


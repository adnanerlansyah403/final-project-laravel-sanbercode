@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header" style="background-color: #06bbcc">
            <h3 class="mb-0 text-light pt-2 pb-2">Question List - {{ $kategori->nama }}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Question</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($post as $item)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $item->judul }}</td>
                            <td>{!! Str::limit($item->pertanyaan, 50, $end='...') !!}</td>
                            {{-- <td>{!! $item->pertanyaan !!}</td> --}}
                            <td>
                                <a href="/post/{{ $item->id }}" class="btn btn-success btn-sm mb-1 btn-block text-light">Show Question</a>
                            </td>
                        </tr>
                        @php
                            $i = 1;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

@push('scripts')

<script src="{{ asset('themes/adminLTE/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('themes/adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
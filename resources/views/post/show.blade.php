@extends('layouts.master')

@push('styles')
    <style>
        #komen_user form:last-child {
            display: none;
        }


        span.trix-button-group.trix-button-group--file-tools {
            display: none;
        }

    </style>
@endpush

@push('styles')
    
    <style>
        /* Comment User */
        div#comment-user form:first-of-type {
            display: block !important;
        }

        div#comment-user form{
            display: none;
        }

    </style>

@endpush

@section('content')
<div class="container-fluid mt-2 mb-4">
    <div class="col-md-12 px-0 w-100 m-auto">
        <div class="card text-center">
            <div class="card-header" style="background-color: #06bbcc">
                <h2 class="mb-0 text-light">Question</h2>
            </div>
            <div class="m-4" style="text-align: justify">
                <div class="text-center m-auto">
                    <img src="{{ asset('img/'. $post->gambar)}}" alt="{{$post->gambar}}" class="img-fluid" style="width: 100%;max-height: 500px;"> 
                </div>
                <h3 class="mt-3 mb-3">{{ $post->judul }}</h3>
                <p class="">{!! $post->pertanyaan !!}</p>
            </div> 
        </div>

        <div class="d-flex flex-column comment-section">
            <div class="card">
                <div class="card-body pl-1 pr-1">
                    @forelse ($post->comment as $item)
                    {{-- {{ dd([$item->id, $comment->user_id, $item]) }} --}}
                    {{-- {{ $comment[0]->user_id }} --}}
                    {{-- {{ dd($item->user_id) }} --}}
                        <div class="bg-white p-2 border-bottom" id="comment-user">
                            <div class="d-flex flex-row user-info">
                                {{-- {{ $item->user->profile->gambar }} --}}
        
                                @if ($item->user->profile->gambar == "defaultlogo.png")
                                    <img class="rounded-circle" src="{{ asset('/defaultimageuser/defaultlogo.png') }}" width="40" alt="imagedefault">
                                @else
                                    <img class="rounded-circle" src="{{ asset('/imageuser/'.$item->user->profile->gambar) }}" alt="imageuser" width="40">
                                @endif
        
                                <div class="d-flex flex-column justify-content-start ml-2"><span class="d-block font-weight-bold name">{{ $item->user->name }}</span><span class="date text-black-50">
                                    Shared publicly - {{ $item->created_at }}
                                </span></div>
        
                            </div>
                            <div class="mt-2 ">
                                <p class="comment-text">{!! $item->comment !!}</p>
                            </div>
                                
                            @foreach ($comment as $c)
                                @if ($c->user_id == $item->user_id)
                                    <form action="/comment/{{ $item->id }}" method="POST">
                                        @csrf
                                        @method("delete")
        
                                        <div class="d-flex justify-content-end">
                                            <a href="/comment/{{ $item->id }}/edit" class="btn btn-warning btn-sm mr-2 text-light">Edit</a>
                                            <button type="submit" class="btn btn-danger btn-sm text-light">Delete</button>
                                        </div>
                                    </form>
                                @else
                                    
                                @endif
                            @endforeach
                                
                        </div>
                    @empty
                        <div class="bg-white">
                            <div class="d-flex flex-row fs-12">
                                <div class="like p-2 cursor"><i class="fa fa-commenting-o"></i><span class="ml-1"><h3>Tidak ada komentar.</h3></span></div>
                            </div>
                        </div>
                    @endforelse
        
                    <div class="bg-white">
                        <div class="d-flex flex-row fs-12">
                            <div class="like p-2 cursor"><i class="fa fa-commenting-o"></i><span class="ml-1">Comment</span></div>
                        </div>
                    </div>
                    <div class="bg-light p-2">
                        <div class="d-flex flex-row align-items-start" >
                            {{-- <img class="rounded-circle" src="https://i.imgur.com/RpzrMR2.jpg" width="40"> --}}
                            <div class="mt-2 text-right" style="width: 100%;">
                                
                                <form class="form-group" action="/comment" method="POST">
                                    @csrf
                                        <input type="hidden" name="post_id" value="{{ $post->id }}" id="">
                                        <div class="form-group">
                                            <input class="@error('comment') is-invalid @enderror text-left" id="comment" name="comment" type="hidden" name="content" />
                                            <trix-editor input="comment" class="@error('comment') is-invalid @enderror form-control text-left" placeholder="Your comment ...">
                                            </trix-editor>
                                        </div>
        
                                        @error('comment')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
        
                                        <button type="submit" class="btn btn-sm shadow-none text-light" type="button" style="background-color: #06bbcc;">
                                            <i class="fas fa-send"></i>
                                            Post comment
                                        </button>
                                        <a href="/post" class="btn btn-sm ml-1 shadow-none text-light" style="background-color: #06bbcc">
                                            Back to main question
                                            <i class="fas fa-chevron-circle-right"></i>
                                        </a>
                                </form>
        
                        </div>
                    </div>
                </div>

                </div>
            </div>

    </div>
</div> 


@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

<script type="text/javascript">

    $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this question?`,
              text: "If you delete this, it will be gone forever.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });

</script>
@endpush



@endsection
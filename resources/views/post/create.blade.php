@extends('layouts.master')

@section('link-post')
<style>
  button.trix-button--icon-attach {
    display: none;
  }

  span.trix-button-group--file-tools {
    display: none;
  }

</style>
@endsection

@section('content')
@auth
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-header" style="background-color: #06bbcc">
        <h3 class="mb-0 text-light">Create Question</h3>
      </div>
      <div class="card-body">
        <form class="forms-sample" action="/post" method="post" enctype="multipart/form-data">
          @csrf
          {{-- <input type="hidden" name="post_id" value="{{ $post->id }}"> --}}
          <div class="form-group">
            <label for="judul" style="font-weight: 600">Title</label>
            <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror" id="judul" placeholder="Judul Pertanyaan" value="{{ old('judul') }}">
          
            @error('judul')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>

          <div class="form-group">
            <label for="pertanyaan" style="font-weight: 600" >Question Description</label>
            <input class="@error('pertanyaan') is-invalid @enderror" id="pertanyaan" name="pertanyaan" type="hidden" name="content" value="{{ old('pertanyaan') }}" />
            <trix-editor input="pertanyaan" class="@error('pertanyaan') is-invalid @enderror"></trix-editor>

            @error('pertanyaan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>
          <div class="form-group">
            <label class="form-label" for="customFile" style="font-weight: 600">Upload Image (optional)</label>
            <input type="file" class="form-control" id="customFile" name="gambar" />
          </div>
          <div class="form-group">
            <label style="font-weight: 600">Category</label>
            <select class="form-control @error('kategori_id') is-invalid @enderror" id="kategori_id" name="kategori_id" value="{{ old('kategori_id') }}">
              <option value="">--- Pilih Opsi Kategori ---</option>
              @foreach ($kategori as $item)
              <option value="{{ $item->id }}">{{ $item->nama }}</option>
              @endforeach
            </select>

            @error('kategori_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>
          <button type="submit" class="btn btn-primary mr-2" style="background-color:#06bbcc; border-style:none">Tambahkan</button>
          <a href="/post" class="btn btn-light">Cancel</a>
        </form>
      </div>
    </div>
  </div>
@endauth

@endsection
@extends('layouts.master')

@section('link-post')
<style>
  button.trix-button--icon-attach {
    display: none;
  }

  span.trix-button-group--file-tools {
    display: none;
  }

</style>
@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <form class="forms-sample" action="/post/{{ $post->id }}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label for="judul" style="font-weight: 600">Judul Pertanyaan</label>
            <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror" id="judul" value="{{ $post->judul }}" required>

            @error('judul')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>

          <div class="form-group">
            <label for="pertanyaan" style="font-weight: 600" >Pertanyaan</label>
            <input id="pertanyaan" name="pertanyaan" type="hidden" name="content" value="{{ $post->pertanyaan }}" required/>
            <trix-editor input="pertanyaan" class="@error('pertanyaan') is-invalid @enderror"></trix-editor>

            @error('pertanyaan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>
          <div class="form-group">
            <label class="form-label" for="customFile" style="font-weight: 600">Lampirkan gambar (optional)</label>
            <input type="file" class="form-control" id="customFile" name="gambar" />
          </div>
          <div class="form-group">
            <label style="font-weight: 600">Kategori</label>
            <select class="form-control @error('name') is-invalid @enderror" id="kategori_id" name="kategori_id" required>
              <option value="">--- Pilih Kategori ---</option>
                @foreach ($kategori as $item)
                  @if ($item->id == $post->kategori_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                  @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                  @endif
                @endforeach
            </select>

            @error('kategori_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>
          <button type="submit" class="btn btn-primary mr-2" style="background-color:#06bbcc; border-style:none">Edit</button>
          <button class="btn btn-light">Cancel</button>
        </form>

      </div>
    </div>
  </div>

@endsection
@extends('layouts.master')

@section('menu-table')
    Categories
@endsection

@section('menu-form')
    Question
@endsection

@push('styles')
    
    <style>

        span.trix-button-group.trix-button-group--file-tools {
            display: none;
        }

    </style>

@endpush

@section('content')
@auth
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <form class="forms-sample" action="/comment/{{ $comment->id }}" method="post">
          @csrf
          @method("PUT")
          <input type="hidden" name="comment_id" value="{{ $comment->id }}">

          <div class="form-group">
            <label for="comment" style="font-weight: 600" >Comment</label>
            <input class="@error('comment') is-invalid @enderror" id="comment" name="comment" type="hidden" name="content" value="{{ $commentuser }}" />
            <trix-editor input="comment" class="@error('comment') is-invalid @enderror" value="{{ $commentuser }}">
            </trix-editor>

            @error('comment')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

          </div>

          <button type="submit" class="btn btn-primary mr-2" style="background-color:#06bbcc; border-style:none">Update Comment</button>
          <a href="/post" class="btn btn-light">Cancel</a>
        </form>
      </div>
    </div>
  </div>
@endauth

@endsection
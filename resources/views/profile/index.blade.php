@extends('layouts.master')

@section('title')
    Profile
@endsection

@section('menu-table')
    {{ Auth::user()->name }}
@endsection

@section('menu-form')
    Account
@endsection

@section('content')
<div class="row">
    <div class="col-lg-10 m-auto">
        <div class="card">
            <div class="card-header" style="background-color: #06bbcc">
                <h4 class="card-title mb-0 text-light p-2">Edit Profile</h4>
            </div>
        
            <div class="card-body">

                @if (session('successupdate'))

                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('successupdate') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                @endif

                <div class="row">
                    <div class="col-md-6">

                    <form action="/profile/{{ Auth::user() }}" method="POST" enctype="multipart/form-data">

                        @csrf
                        @method("PUT")

                            <div class="form-group">
                                <label for="fullName">
                                    <h5>Full name</h5>
                                </label>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="fullName"
                                    placeholder="Full Name ..." value="{{ Auth::user()->name }}">
                            
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="form-group">
                                <label for="image">
                                    <h5>Foto Profile</h5>
                                    {{-- <img src="{{ asset('/imageuser/'. Auth::user()->profile->gambar) }}" alt="" class="img-fluid" style="width: 60px; height: 60px; border-radius:100%; object-fit: cover;"> --}}
                                </label>
                                <input type="file" name="gambarprofile" class="form-control @error('gambarprofile') is-invalid @enderror" id="image"
                                    placeholder="Foto Profile ...">

                                @error('gambarprofile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="tanggallahir">
                                    <h5>Tanggal Lahir</h5>
                                </label>
                                <input type="date" name="tanggallahir" class="form-control" id="tanggallahir" value="{{ Auth::user()->profile->tanggal_lahir }}">
                                @error('tanggallahir')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label for="email">
                                    <h5>Email</h5>
                                </label>
                                <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                    placeholder="Email ..." value="{{ Auth::user()->email }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="tempatlahir">
                                    <h5>Tempat Lahir</h5>
                                </label>
                                <input type="text" name="tempatlahir" class="form-control @error('tempatlahir') is-invalid @enderror" id="tempatlahir"
                                    placeholder="Tempat Lahir ..." value="{{ Auth::user()->profile->tempat_lahir }}">
                                @error('tempatlahir')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">
                                    <h5>Password</h5>
                                </label>
                                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password"
                                    placeholder="Password ..." value="">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        <button type="submit" class="btn btn-block text-light" style="background-color: #06bbcc"><h5>Update</h5></button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.master')

@section('menu-table')
    Categories
@endsection

@section('menu-form')
    Question
@endsection

@section('content')
@yield('button-back')
    <div class="card">
        <div class="card-header bg-primary">
            <h3 class="mb-0 text-light pt-2 pb-2">@yield('title-view-category')</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered text-center">
                <thead>
                    @yield('thead')
                </thead>
                <tbody>
                    @yield('tbody')
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

@push('scripts')

<script src="{{ asset('themes/adminLTE/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('themes/adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush


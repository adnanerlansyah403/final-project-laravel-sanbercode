@extends('layouts.master')

@section('menu-table')
    Categories
@endsection

@section('menu-form')
    Question
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-8 m-auto">
            <form action="/kategori" method="post">
                @csrf
                <div class="card" style="border-radius: 15px;">
                    <div class="card-header" style="border-radius: 15px 15px 0 0;background-color: #06bbcc;">
                        <h2 class="mb-0 text-light text-center">Create data category</h2>
                    </div>
                    <div class="card-body">

                        <div class="mb-3">
                            <label for="nama" class="form-label">
                                <h4 class="mb-0">Category Name</h4>
                            </label>
                            <input type="text" name="nama" class="form-control" id="nama">
                        </div>

                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="mb-3">
                            <label for="deskripsi" class="form-label">
                                <h4 class="mb-0">Description</h4>
                            </label>
                            <textarea name="deskripsi" class="form-control" id="deskripsi" rows="3"></textarea>
                        </div>

                        @error('deskripsi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <button type="submit" class="btn btn-block" style="background-color: #06bbcc">
                            <h3 class="mb-0 text-light">Create</h3>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@extends('layouts.master')

@section('menu-table')
    Categories
@endsection

@section('menu-form')
    Question
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-8 m-auto">
            <form action="/kategori/{{ $kategori->id }}" method="post">
                @csrf
                @method('put')
                <div class="card" style="border-radius: 15px;">
                    <div class="card-header bg-primary" style="border-radius: 15px 15px 0 0;">
                        <h2 class="mb-0 text-light text-center">Edit data category</h2>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="nama" class="form-label">
                                <h4 class="mb-0">Category Name</h4>
                            </label>
                            <input type="text" name="nama" class="form-control" id="nama" value="{{ $kategori->nama }}">
                        </div>

                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="mb-3">
                            <label for="deskripsi" class="form-label">
                                <h4 class="mb-0">Description</h4>
                            </label>
                            <textarea name="deskripsi" class="form-control" id="deskripsi" rows="3">{{ $kategori->deskripsi }}</textarea>
                        </div>

                        @error('deskripsi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <button type="submit" class="btn btn-primary btn-block">
                            <h3 class="mb-0">Update</h3>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@extends('kategori.masterkategori')

@section('content')
<div class="card">
    <div class="card-header" style="background-color: #06bbcc">
        <h3 class="mb-0 text-light pt-2 pb-2">Category List</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered text-center">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Category name</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($kategori as $item)
                <tr>
                    <td>{{ $i }}</td>
                    <td>
                        <a href="/kategori/{{ $item->id }}">{{ $item->nama }}</a>
                    </td>
                    <td>{!! Str::limit($item->deskripsi, 50, $end='...') !!}</td>
                    {{-- <td>{{ $item->deskripsi }}</td> --}}
                    <td>
                        {{ $item->created_at }}
                    </td>
                    <td>
                        {{ $item->updated_at }}
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection
@push('scripts')
<script src="{{ asset('themes/AdminLTE/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('themes/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script>

    $(function () {
        $("#example1").DataTable();
    });

    $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this question?`,
              text: "If you delete this, it will be gone forever.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
</script>
@endpush
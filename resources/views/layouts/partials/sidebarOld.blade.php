<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav sidebar position-fixed">
      <li class="nav-item">
        <div class="d-flex sidebar-profile">
          <div class="sidebar-profile-image">
            @if (Auth::user()->profile->gambar == "defaultlogo.png")
              <img src="{{ asset('/defaultimageuser/defaultlogo.png') }}" alt="image">
            @else
              <img src="{{ asset('/imageuser/'. Auth::user()->profile->gambar) }}" alt="image">
            @endif
            <span class="sidebar-status-indicator"></span>
          </div>
          <div class="sidebar-profile-name">
            <p class="sidebar-name">
              {{ Auth::user()->name }}
            </p>
            <p class="sidebar-designation">
              Welcome
            </p>
          </div>
        </div>

        <p class="sidebar-menu-title">Dash menu</p>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="fas fa-tachometer-alt"></i>
          <span class="menu-title">
            <h4 class="m-0">Dashboard</h4>
          </span>
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#kategori" aria-expanded="false" aria-controls="kategori">
          <i class="fas fa-layer-group"></i>
          <span class="menu-title">
            <h4 class="m-0">Categories</h4>
          </span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="kategori">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="/kategori/mycategory">My Category</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="{{ route('kategori.index') }}">All Category</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="{{ route('kategori.create') }}">Create Category</a>
            </li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#question" aria-expanded="false" aria-controls="question">
          <i class="fas fa-quote-right"></i>
          <span class="menu-title">
            <h4 class="m-0">Question</h4>
          </span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="question">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="/post/myquestion">My question</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('post.index') }}">All question</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="{{ route('post.create') }}">Create question</a>
            </li>
          </ul>
        </div>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="{{ route('profile.index') }}">
          <i class="fas fa-user-cog"></i>
          <span class="menu-title">
            <h4 class="m-0">Profile</h4>
          </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i>
          <span class="menu-title">
            {{ __('Logout') }}
          </span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </li>

    </ul>
</nav>
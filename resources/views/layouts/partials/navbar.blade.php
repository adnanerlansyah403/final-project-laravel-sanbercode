<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="#">
      <img src="{{ asset('landingpage/img/logo/logov2.png') }}" alt="logo">
    </a>
    <button class="navbar-toggler navbar-toggler align-self-center d-none d-lg-flex" type="button" data-toggle="minimize">
      <span class="typcn typcn-th-menu"></span>
    </button>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item nav-profile dropdown">
        <a class="nav-link dropdown-toggle btn btn-sm h-25 d-flex justify-content-center align-items-center text-light" href="#" data-toggle="dropdown" id="plus" style="background-color: #06bbcc">
          <i class="mdi mdi-library-plus mr-0"></i>
          <h4 class="mb-0 ml-1">Create New</h4>
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="plus">
          <a href="/kategori/create" class="dropdown-item">
          <i class="mdi mdi-puzzle" style="color: #06bbcc"></i>
          Create Category
          </a>
          <a href="/post/create" class="dropdown-item">
          <i class="mdi mdi-cloud-print" style="color: #06bbcc"></i>
          Create Question
          </a>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="typcn typcn-th-menu"></span>
    </button>
  </div>
</nav>
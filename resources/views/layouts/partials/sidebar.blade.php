<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <div class="d-flex sidebar-profile">
        <div class="sidebar-profile-image">
          @if (Auth::user()->profile->gambar == "defaultlogo.png")
          <img src="{{ asset('/defaultimageuser/defaultlogo.png') }}" alt="image">
          @else
            <img src="{{ asset('/imageuser/'. Auth::user()->profile->gambar) }}" alt="image">
          @endif
          <span class="sidebar-status-indicator"></span>
        </div>
        <div class="sidebar-profile-name">
          <p class="sidebar-name">
            {{ Auth::user()->name }}
          </p>
          <p class="sidebar-designation">
            Welcome
          </p>
        </div>
      </div>
      <p class="sidebar-menu-title">Dash menu</p>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/kategori">
        <i class="typcn typcn-device-desktop menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <i class="mdi mdi-puzzle menu-icon"></i>
        <span class="menu-title">Categories</span>
        <i class="typcn typcn-chevron-right menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-basic">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="/kategori/mycategory">My Category</a></li>
          <li class="nav-item"> <a class="nav-link" href="/kategori">All Category</a></li>
          <li class="nav-item"> <a class="nav-link" href="/kategori/create">Create Category</a></li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
        <i class="mdi mdi-note-text menu-icon"></i>
        <span class="menu-title">Questions</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="form-elements">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"><a class="nav-link" href="/post/myquestion">My Question</a></li>
          <li class="nav-item"><a class="nav-link" href="/post">All Question</a></li>
          <li class="nav-item"><a class="nav-link" href="/post/create/">Create Question</a></li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/profile">
        <i class="mdi mdi-human-greeting
        menu-icon"></i>
        <span class="menu-title">Profile</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
        <i class="mdi mdi-login-variant menu-icon"></i>
        <span class="menu-title">{{ __('Logout') }}</span>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    </li>
</nav>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title-site')</title>
    <!-- base:css -->
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
    <!-- endinject --> 
    
    <!-- theme css for this page -->
    <link rel="stylesheet" href="{{ asset('/themes/celestial/vendors/typicons.font/font/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('/themes/celestial/vendors/css/vendor.bundle.base.css') }}">
    <!-- End theme css for this page -->

    <!-- icons:css -->
    <link rel="stylesheet" href="{{ asset('/icons/mdi/css/materialdesignicons.min.css') }}">
    <!-- iconsend --> 

    <!-- fonts css for this page -->

    <link rel="stylesheet" href="{{ asset('fonts/fontawesome-free-6/css/all.min.css') }}">

    <!-- End fonts css for this page -->

    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('/themes/celestial/css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="stylesheet" href="{{ asset('/css/trix.css') }}">

    {{-- FavIcon --}}

    @stack('styles')


  </head>
  <body>
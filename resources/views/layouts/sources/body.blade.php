<!-- base:js -->

<script src="{{ asset('/js/script.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="{{ asset('/themes/celestial/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('/themes/celestial/js/off-canvas.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/template.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/settings.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <script src="{{ asset('/themes/celestial/vendors/progressbar.js/progressbar.min.js') }}"></script>
    <script src="{{ asset('/themes/celestial/vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('/themes/celestial/js/dashboard.js') }}"></script>
    <!-- End custom js for this page-->
    <script src="{{ asset('js/trix.js') }}"></script>
    <script src="{{ asset('js/attachments.js') }}"></script>
    
    @include('sweetalert::alert')
    
  </body>
</html>
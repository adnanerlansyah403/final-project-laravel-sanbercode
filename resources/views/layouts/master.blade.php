<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Q&A Sanbercode | {{ $title }}</title>

    <title>@yield('title')</title>  
    <!-- base:css -->

    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
    
    <link rel="stylesheet" href="{{ asset('/themes/celestial/vendors/typicons.font/font/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('/themes/celestial/vendors/css/vendor.bundle.base.css') }}">

    <link rel="stylesheet" href="{{ asset('/icons/mdi/css/materialdesignicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('fonts/fontawesome-free-6/css/all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/themes/celestial/css/vertical-layout-light/style.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/celestial/vendors/simple-datatables/style.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/trix.css') }}">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    {{-- Admin LTE --}}
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('themes/AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/AdminLTE/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    @yield('link-post')
    @stack('overflow')
    @stack('styles')

  </head>
  <body>
    <div class="container-scroller">
      @include('layouts.partials.navbar')

      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        @include('layouts.partials.setting_panel')
        <!-- partial -->
        @include('layouts.partials.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-sm-6">

                <ul class="navbar-nav mb-3">
                  <li class="nav-item font-weight-semibold d-lg-block ms-0">
                    <h1 class="welcome-text">
                      <?php
                          // echo now();
                          // echo date("H");
                          if(date('H') > 11 ) {
                            echo "Selamat Siang,";
                          } else if(date('H') > 18) {
                            echo "Selamat Malam";
                          } else {
                            echo "Selamat Pagi";
                          }
                      ?>
                      <span class="text-black fw-bold" style="color: #06bbcc">{{ Auth::user()->name }}
                      </span>
                    </h1>
                    {{-- <h3 class="welcome-sub-text">Your performance summary this week </h3> --}}
                  </li>
                </ul>
                {{-- <p class="text-last-login">Your last login: 21h ago from newzealand.</p> --}}
              </div>
              <div class="col-sm-6">
                <div class="d-flex align-items-center justify-content-md-end">
                  <div class="mb-3 mb-xl-0 pr-1">
                      
                  </div>
                </div>
              </div>
            </div>

          {{-- Content --}}

            @yield('content')

          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-center text-sm-left d-block d-sm-inline-block">Copyright © <a href="#" style="color: #06bbcc;">Q&A Sanbercode</a> 2022</span>
              {{-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard </a>templates from Bootstrapdash.com</span> --}}
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- base:js -->

    <script src="{{ asset('/js/script.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="{{ asset('/themes/celestial/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('/themes/celestial/js/off-canvas.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/template.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/settings.js') }}"></script>
    <script src="{{ asset('/themes/celestial/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <script src="{{ asset('/themes/celestial/vendors/progressbar.js/progressbar.min.js') }}"></script>
    <script src="{{ asset('/themes/celestial/vendors/chart.js/Chart.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('/themes/celestial/js/dashboard.js') }}"></script>
    <!-- End custom js for this page-->
    <script src="{{ asset('js/trix.js') }}"></script>
    <script src="{{ asset('js/attachments.js') }}"></script>
    @stack('scripts')

    @include('sweetalert::alert')
  </body>
</html>
@extends('layouts.sources.head')

@section('title-site')
    Login
@endsection

<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
        <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
            <div class="brand-logo">
                <img src="{{ asset("/landingpage/img/logo/logov2.png") }}" alt="logo" class="w-100">
            </div>
            <form method="POST" action="{{ route('login') }}" class="pt-3">
                @csrf
                <div class="form-group">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                    @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mt-3">
                    <button type="submit" class="btn btn-block text-light" style="background-color: #06bbcc;"><h4 class="mb-0">SIGN IN</h4></button>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">

                </div>
                <div class="mb-2">
                </div>
                <div class="text-center mt-4 font-weight-light">
                    <span>
                        Dont have an account ?
                        <a href="/register">Register Now.</a>
                    </span>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>
    <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>

<a href="/" class="btn btn-info position-fixed border-none d-inline-flex align-items-center" style="bottom: 30px; left: 30px; background:#06bbcc;border:none;">
    <i class="mdi mdi-arrow-left pr-2"></i>
    Kembali ke halaman utama
</a>



@extends('layouts.sources.body')

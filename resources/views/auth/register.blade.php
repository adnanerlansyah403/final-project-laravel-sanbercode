@extends('layouts.sources.head')

@section('title-site')
    Register
@endsection

@push('')

    <style>
        label {
            font-size: 16px !important;
            font-weight: 400;
        }
    </style>


@endpush

<div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
            <div class="col-lg-4 mx-auto">
                <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                <div class="brand-logo">
                    <img src="{{ asset("/landingpage//img/logo/logov2.png") }}" alt="logo" width="" class="w-100">
                </div>

                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        
                        <label for="nama">Nama</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="form-group">
                        
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" value="{{ old('email') }}">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control @error('email') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">
                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="tanggallahir">Tanggal Lahir</label>
                        <input type="date" name="tanggallahir" class="form-control form-control-lg @error('email') is-invalid @enderror" id="tanggallahir" placeholder="" value="{{ old('tanggallahir') }}" required>
                        @error('tanggallahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">

                        <label for="tempatlahir">Tempat Lahir</label>
                        <input type="text" name="tempatlahir" class="form-control form-control-lg @error('tempatlahir') is-invalid @enderror" id="tempatlahir" value="{{ old('tempatlahir') }}">

                        @error('tempatlahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="form-group"> 
                        <label for="jeniskelamin">Jenis Kelamin</label> <br>
                        <select name="jeniskelamin" class="form-control @error('tempatlahir') is-invalid @enderror" id="jeniskelamin">
                            <option value="">--- Pilih jenis kelamin ---</option>
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                        @error('jeniskelamin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group mb-3">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" rows="3" value="{{ old('alamat') }}"></textarea>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="image">
                            <h5>Foto Profile</h5>
                        </label>
                        <input type="file" name="gambarprofile" class="form-control @error('gambarprofile') is-invalid @enderror" id="image"
                            placeholder="Foto Profile ...">
                        @error('gambarprofile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mt-3">
                        <button type="submit" class="btn btn-block text-light" style="background-color: #06bbcc">
                            <h4 class="mb-0">SIGNUP</h4>
                        </button>
                    
                    </div>

                    <div class="text-center mt-4 font-weight-light">
                        Already have an account?
                        <a href="/login" class="text-decoration-none">Login Now</a>
                    </div>

                    
                </form>
                </div>
            </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    
@extends('layouts.sources.body')
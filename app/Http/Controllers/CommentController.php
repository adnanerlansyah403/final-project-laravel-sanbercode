<?php

namespace App\Http\Controllers;

use App\User;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CommentController extends Controller
{
    //

    public function comment(Request $request)
    {

        $request->validate([
            'comment' => 'required|max:1000',
        ],[
            'comment.required' => 'Komentar wajib di isi.',
        ]);

        $comment = new Comment;
        $comment->comment = $request->comment;
        $comment->user_id = Auth::id();
        $comment->post_id = $request->post_id;

        $comment->save();

        return redirect('/post/'.$request->post_id);
    }

    public function edit($id)
    {   
        $title = 'Comment';
        $comment = Comment::where('id', $id)->get()->first();
        $commentuser = strip_tags($comment->comment);
        $user = User::where('id', Auth::id())->get()->first();


        return view('comment.edit', compact('title', 'comment', 'commentuser','user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'comment' => 'required|max:1000',
        ],[
            'comment.required' => 'Comment harus di isi.',
        ]);

        Comment::where('id', $id)->get()->first()
            ->update([
                'comment' => $request['comment'],
            ]);

        Alert::success("Success", "Berhasil memperbarui komentar.");

        return redirect('/post');
    }

    public function destroy($id)
    {
        Comment::find($id)->delete();
        Alert::success("Success", "Berhasil menghapus pertanyaan.");  

        return back();
    }

}

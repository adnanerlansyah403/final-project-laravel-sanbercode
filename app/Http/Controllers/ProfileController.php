<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        // $user = User::where('id', Auth::id())
        //             ->get()
        //             ->first();

        // dd($user->profile);

        return view('profile.index', [
            'title' => 'Profile'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // $profile = Profile::where('user_id', Auth::id())->get()->first();
        // dd($profile->tanggal_lahir);

        $request->validate([
            'name' => ['required', 'string', 'max:255', 'nullable'],
            'email' => ['required', 'string', 'email', 'max:255', 'nullable'],
            'password' => ['min:8','nullable'],
            'tanggallahir' => ['required', 'nullable'],
            'tempatlahir' => ['required'],
            'gambarprofile' => 'mimes:png,jpg,jpeg|max:1024',
        ],[
            'tanggallahir.required' => 'Tanggal Lahir harus di isi.',
            'tempatlahir.required' => 'Tempat Lahir harus di isi.',
            'alamat.required' => 'Alamat harus di isi.',
            // 'gambarprofile.required' => 'Gambar harus di masukkan.',
            'gambarprofile.image' => 'Gambar harus ber-ekstensi JPG,JPEG,dan PNG.',
            'gambarprofile.max' => 'Ukuran gambar maximal : 1024mb',
        ]);

        if( isset($request['gambarprofile']) ) {

            // dd($request['gambarprofile']);
            // dd($profile->tempat_lahir);

            
            
            $fileName = 'User-' . time(). '.' . $request['gambarprofile']->extension();
        
            $user = User::where('id', Auth::id())
                ->update([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password' => Hash::make($request['password']),
                ]);

            $profile = Profile::where('user_id', Auth::id())->get()->first()
                ->update([
                    'tanggal_lahir' => $request['tanggallahir'],
                    'tempat_lahir' => $request['tempatlahir'],
                    'gambar' => $fileName,
                ]);

            

            $request['gambarprofile']->move(public_path('imageuser'), $fileName);

            Alert::success('Success', 'Berhasil memperbarui data profile.');

            return redirect('profile');
            

        } else if( !isset($request['gambarprofile']) ) {

            $profile = Profile::where('user_id', Auth::id())->get()->first();

            // dd($profile->gambar);

            if( $profile->gambar != "defaultlogo.png" ) {
                $fileName = $profile->gambar;

                $user = User::where('id', Auth::id())
                    ->update([
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'password' => Hash::make($request['password']),
                    ]);

                $profile = Profile::where('user_id', Auth::id())->get()->first()
                    ->update([
                        'tanggal_lahir' => $request['tanggallahir'],
                        'tempat_lahir' => $request['tempatlahir'],
                        'gambar' => $fileName,
                    ]);
                

                Alert::success('Success', 'Berhasil memperbarui data profile.');

                return redirect('profile');

            } else {
                
                $fileName = "defaultlogo.png";

                $user = User::where('id', Auth::id())
                    ->update([
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'password' => Hash::make($request['password']),
                    ]);

                $profile = Profile::where('user_id', Auth::id())->get()->first()
                    ->update([
                        'tanggal_lahir' => $request['tanggallahir'],
                        'tempat_lahir' => $request['tempatlahir'],
                        'gambar' => $fileName,
                    ]);
                

                Alert::success('Success', 'Berhasil memperbarui data profile.');

                return redirect('profile');
            }

        }

        
    }

}

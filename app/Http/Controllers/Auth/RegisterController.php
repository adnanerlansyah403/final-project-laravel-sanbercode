<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'tanggallahir' => ['required'],
            'tempatlahir' => ['required', 'string'],
            'jeniskelamin' => ['required'],
            'alamat' => ['required'],
            'gambarprofile' => 'mimes:jpeg,jpg,png|max:1024',
        ],[
            'tanggallahir.required' => 'Tanggal Lahir wajib di isi.',
            'tempatlahir.required' => 'Tempat Lahir wajib di isi.',
            'jeniskelamin.required' => 'Jenis Kelamin wajib di isi.',
            'alamat.required' => 'Alamat wajib di isi.',
            // 'gambarprofile.required' => 'Foto Profile wajib di masukkan.',
            'gambarprofile.image' => 'Gambar harus ber-ekstensi JPG,JPEG,dan PNG.',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        // dd($data['gambarprofile']);
        // dd($data['gambarprofile']->extension());

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        if( isset($data['gambarprofile']) ) {

            // dd($data['gambarprofile']);

            $fileName = 'User-' . time(). '.' . $data['gambarprofile']->extension();

            Profile::create([
                'tanggal_lahir' => $data['tanggallahir'],
                'tempat_lahir' => $data['tempatlahir'],
                'jenis_kelamin' => $data['jeniskelamin'],
                'alamat' => $data['alamat'],
                'gambar' => $fileName,
                'user_id' => $user->id,
            ]);
    
            $data['gambarprofile']->move(public_path('imageuser'), $fileName);
    
            return $user;
        } else {

            $fileName = "defaultlogo.png";

            Profile::create([
                'tanggal_lahir' => $data['tanggallahir'],
                'tempat_lahir' => $data['tempatlahir'],
                'jenis_kelamin' => $data['jeniskelamin'],
                'alamat' => $data['alamat'],
                'gambar' => $fileName,
                'user_id' => $user->id,
            ]);

            return $user;
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
// use App\Auth;
use App\Comment;
use App\Profile;
use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use \Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Create Personal Controller

    public function index()
    {
        $title = "Question List";
        $post = Post::all();
        // $pertanyaan = Str::limit($post->pertanyaan, 5, $end='...');
        return view('post.index', compact('post', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create Question";
        $kategori = Kategori::all();
        return view('post.create', compact('kategori', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nm = $request->gambar;
        if ($request->gambar == is_null(true)) {
            $namaFile = '';
        } else {
            $namaFile = $nm->getClientOriginalName();
            $nm->move(public_path().'/img', $namaFile);
        }


        $request->validate([
            'judul' => ['required'],
            'pertanyaan' => ['required'],
            'kategori_id' => ['required'],
        ],[
            'judul.required' => 'Judul harus di isi.',
            'pertanyaan.required' => 'Pertanyaan harus di isi.',
            'kategori_id.required' => 'Kategori harus di masukkan.',
        ]);

        Post::create([
            'judul' => $request['judul'],
            'gambar' => $namaFile,
            'pertanyaan' => $request['pertanyaan'],
            // 'pertanyaan' => strip_tags($request['pertanyaan']),
            'kategori_id' => $request['kategori_id'],
            'user_id' => Auth::id(),
        ]);

        Alert::success("Success", "Anda berhasil membuat pertanyaan.");

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Question Post";
        $post = Post::where('id', $id)->get()->first();
        $pertanyaan = $post->pertanyaan;
        // $user = User::where('id', Auth::id())->get()->first();
        // $comment = Comment::where('user_id', Auth::id())->get()->first();
        // return view('post.show', compact('title', 'post', 'pertanyaan', 'profile', 'comment'));

        // $profile = Profile::where('id', Auth::id())->get()->first();
        $profile = Profile::where('id', Auth::id())->get();
        // $user = User::where('id', Auth::id())->get()->first();
        // $comment = Comment::where('user_id', Auth::id())->get()->first();
        $comment = Comment::where('user_id', Auth::id())->get();
        // dd(count($comment));
        // dd($comment);
        // print($comment->user_id);

        
        return view('post.show', compact('title', 'post', 'pertanyaan', 'profile', 'comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Question";
        $kategori = Kategori::all();
        $post = Post::where('id', $id)->get()->first();
        return view('post.edit', compact('post', 'kategori', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // @dd($request['gambar']);

        $request->validate([
            'judul' => 'required|max:50',
            'pertanyaan' => 'required',
            'gambar' => 'mimes:jpg,jpeg,png|max:1024',
            'kategori_id' => 'required',
        ],[
            'judul.required' => 'Judul harus di isi.',
            'pertanyaan.required' => 'Pertanyaan harus di isi.',
            'gambar.mimes' => 'Extnsion gambar harus berupa jpg,jpeg,dan png.',
            'gambar.max' => 'Ukuran size gambar maximal 1024 mb.',
            'kategori_id.required' => 'Kategori harus di masukkan.',
        ]);

        $nm = $request['gambar'];
        if ($request['gambar'] == is_null(true)) {
            Post::where('id', $id)->get()->first()
            ->update([
                'judul' => $request['judul'],
                'pertanyaan' => $request['pertanyaan'],
                'kategori_id' => $request['kategori_id'],
            ]);

            Alert::success("Success", "Berhasil memperbarui pertanyaan.");

            return redirect('/post/myquestion');

        } else {
            $namaFile = $nm->getClientOriginalName();
            $nm->move(public_path().'/img', $namaFile);

            Post::where('id', $id)->get()->first()
            ->update([
                'judul' => $request['judul'],
                'pertanyaan' => $request['pertanyaan'],
                'gambar' => $namaFile,
                'kategori_id' => $request['kategori_id'],
            ]);

            Alert::success("Success", "Berhasil memperbarui pertanyaan.");

            return redirect('/post/myquestion');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Post::destroy($id);
        return redirect('/post')->with('success', 'Post successfully removed!');
    }

    public function upload(Request $request){
        if($request->hasFile('file')) {
            //get filename with extension
            $filenamewithextension = $request->file('file')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('file')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('file')->storeAs('public/uploads', $filenametostore);

            // you can save image path below in database
            $path = asset('storage/uploads/'.$filenametostore);

            echo $path;
            exit;
        }

        Post::find($id)->delete();

        Alert::success("Success", "Berhasil menghapus pertanyaan.");  
        return back();
    }

    public function myquestion()
    {
        $title = 'My Question';
        // dd(Auth::id());
        $post = Post::where('user_id', Auth::id())->get();
        // dd($post);
        return view('post.myquestion', compact('post', 'title'));
    }


}

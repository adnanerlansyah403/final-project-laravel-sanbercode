<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // protected $table = 'posts';

    protected $fillable = ['judul', 'pertanyaan', 'gambar', 'kategori_id', 'user_id'];
    // protected $guarded = ['id'];

    public function post()
    {
        return $this->belongsTo('App\User');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }
    
    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    protected $guarded = ['id'];

    // public $timestamps = false;


    public function post()
    {
        return $this->hasOne('App\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}